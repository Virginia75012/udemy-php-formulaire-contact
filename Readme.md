# Cours UDEMY John Codeur

### Apprendre PHP et créer un formulaire de contact

Apprentissage de PHP par la pratique. Tout d'abord révision des bases

Mise en place du serveur APACHE sur Linux (LAMP).

Revoyons les bases :
- syntaxe
- variables
- scopes
- types de variables
- strings (chaînes de caractères)
- constants (constantes)
- operators (opérateurs)
- conditions
- loops (boucles)
- functions (fonctions)
- arrays (tableaux)
- objects (objets)
- include et require
- super globales
- GET et POST
- sessions
- cookies