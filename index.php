<?php

    $firstname = $name = $email = $phone = $message = "";
    $firstnameError = $nameError = $emailError = $phoneError = $messageError = "";

    //Si formulaire envoyé... récupération des données
    if($_SERVER["REQUEST_METHOD"] == "POST")
    {
        $firstname = verifyInput($_POST["firstname"]);
        $name = verifyInput($_POST["name"]);
        $email = verifyInput($_POST["email"]);
        $phone = verifyInput($_POST["phone"]);
        $message = verifyInput($_POST["message"]);

        if(empty($firstname)){

            $firstnameError = "Veuillez saisir votre prénom";
        }

        if(empty($name)){

            $nameError = "Veuillez saisir votre nom";
        }
        if(empty($message)){

            $messageError = "Veuillez écrire quelques mots dans le message avant d'envoyer";
        }
        if(!isEmail($email)){

            $emailError = "Ceci n'est pas un format valide d'adresse mail";
        }
    }

    function isEmail($var)
    {
        return filter_var($var, FILTER_VALIDATE_EMAIL); //filtre PHP de validation d'email
    }

    function verifyInput($var)
    {
        $var = trim($var); //enlever les espaces, les retours à la ligne, etc... des champs input
        $var = stripslashes($var); //enlever tout les anti-slash
        $var = htmlspecialchars($var); // eviter de mettre des tag html dans l'URL, éviter les hackers

        return $var;
    }
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
    <title>Formulaire de Contact</title>
</head>

<body>
<div class="container">
    <div class="divider"></div>
    <div class="heading">
        <h2>Contactez-moi</h2>
    </div>
    <div class="row">
        <div class="col-lg-12 col-lg-offset-1">
            <!-- Informations à renvoyer sur la même page avec $_SERVER['PHP_SELF']-->
            <form id="contact-form" method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>" role="form">
                <div class="row">
                    <div class="col-md-6">
                        <label for="firstname">Prénom<span class="blue"> *</span></label>
                        <input type="text" id="firstname" name="firstname" class="form-control" placeholder="Votre Prénom" value="<?= $firstname ?>">
                        <p class="comments"><?= $firstnameError ?></p>
                    </div>
                    <div class="col-md-6">
                        <label for="name">Nom<span class="blue"> *</span></label>
                        <input type="text" id="name" name="name" class="form-control" placeholder="Votre Nom" value="<?= $name ?>">
                        <p class="comments"><?= $nameError ?></p>
                    </div>
                    <div class="col-md-6">
                        <label for="email">Email<span class="blue"> *</span></label>
                        <input type="mail" id="email" name="email" class="form-control" placeholder="Votre Email" value="<?= $email ?>">
                        <p class="comments"><?= $emailError ?></p>
                    </div>
                    <div class="col-md-6">
                        <label for="phone">Téléphone</label>
                        <input type="tel" id="phone" name="phone" class="form-control" placeholder="Votre Téléphone" value="<?= $phone ?>">
                        <p class="comments"><?= $phoneError ?></p>
                    </div>
                    <div class="col-md-12">
                        <label for="message">Message<span class="blue"> *</span></label>
                        <textarea id="message" name="message" class="form-control" placeholder="Votre Message" row="4"><?= $message ?></textarea>
                        <p class="comments"><?= $messageError ?></p>
                    </div>
                    <div class="col-md-12">
                        <p class="blue"><strong>* Ces informations sont requises</strong></p>
                    </div>
                    <div class="col-md-12">
                        <input type="submit" class="button1" value="Envoyer">
                    </div>
                </div>
                <p class="thank-you">Votre message a bie été envoyé. Merci de m'avoir contacté :-)</p>
            </form>
        </div>
    </div>
</div>


    <script src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
</body>

</html>